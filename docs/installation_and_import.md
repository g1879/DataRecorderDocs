---
hide:
  - navigation
---

<script src="https://cdn.wwads.cn/js/makemoney.js" async></script>
<div class="wwads-cn wwads-horizontal" data-id="317"></div><br/>

## 🎯 安装

```console
pip install DataRecorder
```

## 🎯 导入

### ♥️ 导入记录器

```python
from DataRecorder import Recorder
```

### ♥️ 导入表格填写器

```python
from DataRecorder import Filler
```

### ♥️ 导入二进制数据记录器

```python
from DataRecorder import ByteRecorder
```

### ♥️ 导入数据库记录器

```python
from DataRecorder import DBRecorder
```
